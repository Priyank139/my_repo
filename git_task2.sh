#!/bin/bash

Listbranches () {
    echo "list all branches."
    git branch -a 
}

Createbranch () {
    echo "create a new branch"
    git checkout -b $1
}

Deletebranch () {
    echo "delete a branch"
    git branch -d $1
}

Merge2branches () {
    echo "merge 2 branches"
    git merge $1
}

 Rebase2branches () {
    echo "rebase 2 branches"
    git rebase $1 $2
 }



case "$1" in
"Listbranches")
        Listbranches 
    ;;
"Createbranch")
        Createbranch $2               
    ;;
"Deletebranch")
        Deletebranch $2
    ;;
"Merge2branches")
        Merge2branches $2
    ;;
"Rebase2branches")
        Rebase2branches $2 $3
    ;;
    esac

